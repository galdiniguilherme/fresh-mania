from pymongo import MongoClient
from time import sleep
import cv2


#  stabilishing connection with mongodb
def connect_mongo(object_connection):
    client = MongoClient('mongodb://localhost:27017')
    db = client['FreshMania']
    users = db.users

    user_id = users.insert_one(object_connection).inserted_id


# using opencv lib to display images
def display_image(product):  # check
    print('Opening product image in:')
    for secs in range(4, 0, -1):
        print(secs)
        sleep(1)

    img = cv2.imread(product)
    cv2.imshow(str(product), img)
    cv2.waitKey(0)  # waits until a key is pressed
    cv2.destroyAllWindows()  # destroys the window showing image
