from datetime import datetime

branches_global = ['Osasco', 'São Carlos', 'Araraquara', 'Brasília', 'Paris', 'Cracóvia']
current_time = datetime.now()
f_hour = current_time.strftime("%H:%M")
f_data = current_time.strftime("%d/%m/%Y")


class Store:

    def __init__(self, product_name: str,
                 description: str,
                 prod_ID: str,
                 price: float,
                 image: str,
                 branches: list,
                 amount_sold: int = 0,
                 stock: int = 0,
                 sale_mean_per_day: float = 0.0,
                 sell_leader: bool = False):

        self.product_name = product_name
        self.description = description
        self.prod_ID = prod_ID
        self.price = price
        self.image = image
        self.branches = branches
        self.amount_sold = amount_sold
        self.stock = stock
        self.sale_mean_per_day = sale_mean_per_day
        self.sell_leader = sell_leader

    # function that insert product in store's menu
    def put_in_menu(self, ID):  # check
        if ID == self.prod_ID:
            print(f'The product with ID [{self.prod_ID}]: {self.product_name} '
                  f'is now at sale in our menu. Time: {f_hour} in {f_data}')
        else:
            print('ID not found!')

    # function that remove product from store's menu
    def remove_from_menu(self, ID):  # check
        if ID == self.prod_ID:
            print(f'The product with ID [{self.prod_ID}]: {self.product_name} '
                  f'is out of our menu. Time: {f_hour} in {f_data}')
        else:
            print('ID not found!')

    # function to modify the name of a product, based in your ID
    def modify_product_name(self, ID):  # check
        previous_name = self.product_name
        if ID == self.prod_ID:
            new_name = str(input('New product name: '))
            self.product_name = new_name
            print(f'Name modified from {previous_name} to {self.product_name}!')
        else:
            print('ID not found in database!')

    # function to modify the description of a product, based in your ID
    def modify_description(self, ID):  # check
        previous_description = self.description
        if ID == self.prod_ID:
            new_description = str(input('New description: '))
            self.description = new_description
            print(f'Modifications made with success!\n'
                  f'Previous description: {previous_description}\n'
                  f'New description: {self.description}.')
        else:
            print('ID not found')

    # function to increase the price of a product, based in your ID, and print the previous and new values of him
    def increase_price(self, ID):  # check
        if ID == self.prod_ID:
            addition = float(input('Addition: '))
            before = self.price
            self.price += addition

            print(f'Product [{self.prod_ID}]: {self.product_name} received an addition!')
            print(f'Previous price: {before}R$\n Adjusted price: {self.price} R$')
        else:
            print('ID not found!')

    # function to see all store's branches that a product is part of, based in your ID
    def see_branches(self, ID):  # check
        if ID == self.prod_ID:
            print(self.branches)
        else:
            print('ID not found')

    # function to add the product in a new store's branch, based in your ID (and if the branch exist)
    def add_new_branch(self, ID):  # check
        print(f'Branches available: {branches_global}')
        branch = str(input('Branch request: '))
        if ID == self.prod_ID:
            if branch in branches_global and branch not in self.branches:
                self.branches.append(branch)
                print(f'The product [{self.prod_ID}] '
                      f'{self.product_name} is now part of {branch} menu!\n')
            else:
                print('Branch with this product already!')
        else:
            print('ID not found!')

    # function to see all the store's branches
    def see_store_branches(self):  # check
        print(self.branches)

    # function to sell a product, since amount < stock and stock > 0
    def sell_product(self):  # check
        amount = int(input('Amount: '))
        if self.stock < amount or self.stock == 0:
            print('Insufficent Stock for this operation...')
        else:
            self.stock -= amount
            self.amount_sold += amount
            print('Sale made with success!')

    # function to see the summatory of sales of a product
    def sold_quantity(self):  # check
        if self.amount_sold == 0:
            print('No sales made until now!')
        print(f'Sales amount: {self.amount_sold}')

    # function to increase the stock of a product, based in product ID
    def increase_stock(self, ID):  # check
        if ID == self.prod_ID:
            amount = int(input('Value to add at the stock: '))
            self.stock += amount
            print(f'Stock readjusted!\nStock: {self.stock}')
        else:
            print('ID not found!')

    # function to decrease the stock of a product, based in product ID
    def decrease_stock(self, ID):  # check
        if ID == self.prod_ID:
            amount = int(input('Value to decrease at the stock: '))
            if amount > self.stock:
                print('Stock doenst support this demand!')
            else:
                self.stock -= amount
                print(f'Stock readjusted!\nStock: {self.stock}')
        else:
            print('ID not found!')

    # function to see the sales mean of a product, considering the entire month
    def show_mean(self):  # check
        if self.amount_sold > 0.0:
            print(f'Mean of sales per day: {round(self.amount_sold / 30, 2)}!')
        else:
            print('Not enough sales for this operation')

    # function to see the sales amount of a product, considering him like well ou bad succeeded
    def verify_sales(self):
        if self.sale_mean_per_day >= 20.0:
            self.sell_leader = True
            print(f'Product {self.product_name} is on FIRE!!')
        else:
            self.sell_leader = False
            print(f'Product {self.product_name} is WEAK on sales nowadays!!')

    # return all information of a product (formatted)
    def asdict(self):
        return {'Product Name': self.product_name,
                'Product Description': self.description,
                'Product ID': self.prod_ID,
                'Product Price': self.price,
                'Product Image Name': self.image,
                'Product Sales': self.amount_sold,
                'Product Stock': self.stock,
                'Product Sales Mean': self.sale_mean_per_day,
                'Product Popularity': self.sell_leader}


# Product Class (Store dependent)
# =============================================================================

class Product(Store):

    def __init__(self,
                 product_name: str,
                 description: str,
                 prod_ID: str,
                 price: float,
                 image: str,
                 branches: list,
                 amount_sold: int = 0,
                 stock: int = 0,
                 sale_mean_per_day: float = 0.0,
                 sell_leader: bool = False):
        super().__init__(product_name,
                         description,
                         prod_ID,
                         price,
                         image,
                         branches,
                         amount_sold,
                         stock,
                         sale_mean_per_day,
                         sell_leader)

        self.product_name = product_name
        self.description = description
        self.prod_ID = prod_ID
        self.price = price
        self.image = image
        self.branches = branches
        self.amount_sold = amount_sold
        self.stock = stock
        self.sale_mean_per_day = sale_mean_per_day
        self.sell_leader = sell_leader

    def asdict(self):
        return {'Product Name': self.product_name,
                'Product Description': self.description,
                'Product ID': self.prod_ID,
                'Product Price': self.price,
                'Product Image Name': self.image,
                'Product Sales': self.amount_sold,
                'Product Stock': self.stock,
                'Product Sales Mean': self.sale_mean_per_day,
                'Product Popularity': self.sell_leader}

    def __str__(self):
        return self.product_name, \
               self.description, \
               self.prod_ID, \
               self.price, \
               self.image, \
               self.branches, \
               self.amount_sold, \
               self.stock, \
               self.sale_mean_per_day, \
               self.sell_leader
