from static_defs import *
from product_store import prod, store


def run():
    prod.put_in_menu('CF250601')
    prod.modify_product_name('CF250601')
    prod.modify_description('CF250601')
    prod.remove_from_menu('CF250601')
    prod.increase_price('CF250601')
    prod.see_branches('CF250601')
    prod.add_new_branch('CF250601')
    prod.see_branches('CF250601')
    store.see_store_branches()
    prod.sell_product()
    prod.increase_stock('CF250601')
    prod.sell_product()
    prod.sold_quantity()
    prod.increase_stock('CF250601')
    prod.sell_product()
    prod.sold_quantity()
    prod.increase_stock('CF250601')
    prod.decrease_stock('CF250601')
    prod.verify_sales()
    prod.sell_product()
    prod.verify_sales()
    x = prod.asdict()
    y = store.asdict()
    connect_mongo(x)
    connect_mongo(y)


run()
