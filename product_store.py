from classes import Product, Store
from pymongo import MongoClient
from static_defs import *


prod = Product(
    product_name='Café bolado',
    description='Café de torra média, moído e torrado na hora, com adicional de chantily',
    prod_ID='CF250601',
    price=8.00,
    image='café.jpg',
    branches=['Osasco', 'São Carlos', 'Araraquara'],
    amount_sold=0,
    stock=200,
    sale_mean_per_day=35.0,
    sell_leader=False
)

store = Store(
    product_name='Fresh Mania',
    description='Loja do ramo de vendas de produtos In Natura',
    prod_ID='https://freshmania.com.br/',
    price=250000.00,
    image='fresh.png',
    branches=['Osasco', 'São Carlos', 'Araraquara', 'Brasília', 'Paris', 'Cracóvia'],
    amount_sold=30000,
    stock=20000,
    sale_mean_per_day=200.0,
    sell_leader=False
)

